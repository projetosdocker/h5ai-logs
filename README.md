# h5ai - Logs

Serviço h5ai para verificação de logs

### Persistência de dados

Para persistir os dados é necessário executar o seguinte comando:

```
mkdir -p /srv/persist-services/logs
```

### Serviço

Para criar o serviço, deve ser executada a seguinte linha de comando:

```
docker stack deploy -c stack.yml log
```

Para verificar o serviço deve ser executado o comando:

```
docker service ps log_app
```

Para remove o serviço deve ser executado o comando:

```
docker stack rm log
```
